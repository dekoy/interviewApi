package fakeapi

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"

	"github.com/AyodejiO/fakeapi/dto"
	"github.com/AyodejiO/fakeapi/errs"
)

const (
	DEFAULT_BASE_URL = "http://localhost:8080/v1/organisation/accounts"
)

type Client struct {
	BaseURL string
}

type Params struct {
	RequestBody []byte
	Path *string
	Method string
	ExpectedStatusCode int
	Context context.Context
	ResponseFormat interface{}
}

func (c Client) GetAllAccounts(ctx context.Context, filter dto.Filter) (*dto.AccountCollection, *errs.ApiError) {

	encodedUrl := filter.ToEncodedString()

	accounts, err := c.Request(Params{
		RequestBody: nil,
		Path: &encodedUrl,
		Method: http.MethodGet,
		ExpectedStatusCode: http.StatusOK,
		Context: ctx,
		ResponseFormat: &dto.AccountCollection{},
	})

	if err != nil {
		return nil, err
	}

	return accounts.(*dto.AccountCollection), nil
}

func (c Client) GetSingleAccount(ctx context.Context, id string) (*dto.Account, *errs.ApiError) {

	accountRequest, err := c.Request(Params{
		RequestBody: nil,
		Path: &id,
		Method: http.MethodGet,
		ExpectedStatusCode: http.StatusOK,
		Context: ctx,
		ResponseFormat: &dto.AccountRequest{},
	})

	if err != nil {
		return nil, err
	}

	account := accountRequest.(*dto.AccountRequest).ToAccountObj()

	return &account, nil 
}

func (c Client) CreateAccount(ctx context.Context, a dto.Account) (*dto.Account, *errs.ApiError) {

	body, err := a.ToRequestObj().ToJson()

	if err != nil {
		return nil, errs.NewUnexpectedError(err.Error())
	}

	accountRequest, apiErr := c.Request(Params{
		RequestBody: body,
		Path: nil,
		Method: http.MethodPost,
		ExpectedStatusCode: http.StatusCreated,
		Context: ctx,
		ResponseFormat: &dto.AccountRequest{},
	})

	if apiErr != nil {
		return nil, apiErr
	}

	account := accountRequest.(*dto.AccountRequest).ToAccountObj()

	return &account, nil 
}

func (c Client) DeleteAccount(ctx context.Context, a dto.Account) (*errs.ApiError) {

	path := "/" + a.ID + "?version=" + strconv.FormatInt(*a.Version, 10)
	
	if _, err := c.Request(Params{
		RequestBody: nil,
		Path: &path,
		Method: http.MethodDelete,
		ExpectedStatusCode: http.StatusNoContent,
		Context: ctx,
	}); err != nil {
		return err
	}
	
	return nil 
}

func (c Client) Request(params Params) (interface{}, *errs.ApiError) {

	baseURL := c.getUrl(params.Path)

	client := &http.Client{}
	
	req, err := http.NewRequest(params.Method, baseURL, bytes.NewBuffer(params.RequestBody));

	if err != nil {
		return nil, errs.NewUnexpectedError(fmt.Sprintf("Error creating request: %s", err.Error()))
	}

	req.Header.Add("Content-Type", "application/vnd.api+json")

	if params.Context != nil {
		req = req.WithContext(params.Context)
	}

	response, err := client.Do(req);

	if err != nil {
		return nil, errs.NewUnexpectedError(fmt.Sprintf("Error executing request: %s", err.Error()))
	}
 
	defer response.Body.Close()

	var data bytes.Buffer

	if _, err := io.Copy(&data, response.Body); err != nil {
		return nil, errs.NewUnexpectedError(fmt.Sprintf("Error reading response: %s", err.Error()))
	}

	if response.StatusCode != params.ExpectedStatusCode {
		return nil, errs.NewApiClientError(response.StatusCode, data.String())
	}

	if params.ResponseFormat == nil {
		return nil, nil
	}

	resp := params.ResponseFormat

	if err := json.Unmarshal(data.Bytes(), resp); err != nil {
		return nil, errs.NewUnexpectedError(fmt.Sprintf("Error unmarshalling response: %s", err.Error()))
	}

	return resp, nil
}

func (c Client) getUrl(path *string) string {
	baseURL := c.BaseURL

	if c.BaseURL == "" {
		baseURL = DEFAULT_BASE_URL
	}

	if path != nil {
		if !strings.HasPrefix(*path, "/") {
			baseURL = baseURL + "/" + *path
		} else {
			baseURL = baseURL + *path
		}
	}

	return baseURL
}