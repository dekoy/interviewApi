package fakeapi

import (
	"context"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/AyodejiO/fakeapi/dto"
	"github.com/AyodejiO/fakeapi/errs"
	"github.com/google/uuid"
)

type TestReponse struct {
	Value string
}

func TestClientNewRequest(t *testing.T) {
	path := "/randomPath"
	contentType := "application/vnd.api+json"

	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.URL.Path != path {
					t.Errorf("Expected to request %s, got: %s", path, r.URL.Path)
			}
			if r.Header.Get("Content-Type") != contentType {
					t.Errorf("Expected Content-Type: %s header, got: %s", contentType, r.Header.Get("Content-Type"))
			}
			w.WriteHeader(http.StatusOK)
			w.Write([]byte(`{"value":"ok"}`))
	}))

	defer server.Close()

	client :=  Client{
		BaseURL: server.URL,
	}

	resp, err := client.Request(Params{
		RequestBody: nil,
		Path: &path,
		Method: http.MethodGet,
		ExpectedStatusCode: http.StatusOK,
		Context: context.TODO(),
		ResponseFormat: &TestReponse{},
	})

	v := resp.(*TestReponse)

   if err != nil {
		t.Errorf("response decoding failed")
   }

	if v.Value != "ok" {
			t.Errorf("Expected 'ok', got %s", v.Value)
	}
}

func TestCreateAccountShouldReturnValidationError (t *testing.T) {

	client :=  getClient()

	a := dto.Account{
		ID: uuid.New().String(),
	}

	_, err := createNewAccount(&client, &a)

	if err.Code != http.StatusBadRequest {
		t.Errorf("Expected %v, got %v", http.StatusBadRequest, err.Code)
		t.Errorf("Expected %v, got %v", nil, err.AsMessage())
	}
}

func TestCreateAccountShouldReturnSuccess (t *testing.T) {
	client :=  getClient()

	_, err := createNewAccount(&client, nil)

	if err != nil {
		t.Errorf("Expected nil, got %v", err.AsMessage())
	}
}

func TestGetAllAccountsShouldReturnSuccess (t *testing.T) {

	client :=  getClient()

	params := dto.Filter {
		PageSize: 1,
	}

	accounts, err := client.GetAllAccounts(context.TODO(), params)

	if err != nil {
		t.Errorf("Expected nil, got %v", err)
	}

	if len(accounts.Data) != 1 {
		t.Errorf("Expected %v, got %v", 1, len(accounts.Data))
	}
}

func TestGetAllAccountsShouldWithCanceledContext (t *testing.T) {

	client :=  getClient()

	params := dto.Filter {
		PageSize: 1,
	}

	ctx, cancel := context.WithCancel(context.Background())
	cancel()
	accounts, err := client.GetAllAccounts(ctx, params)

	if err.Code != http.StatusInternalServerError {
		t.Errorf("Expected nil, got %d", err.Code)
	}

	if !strings.Contains(err.AsMessage(), "context canceled")  {
		t.Errorf("Expected context canceled in error, got %s", err.AsMessage())
	}

	if accounts != nil {
		t.Errorf("Expected %v, got %v", nil, accounts)
	}
}

func TestFetchAccountShouldReturnSuccess (t *testing.T) {

	client :=  getClient()

	account, err := createNewAccount(&client, nil)

	if err != nil {
		t.Errorf("Expected nil, got %v", err)
	}

	fetchedAccount, err := client.GetSingleAccount(context.TODO(), account.ID)

	if err != nil {
		t.Errorf("Expected nil, got %v", err.AsMessage())
	}

	if account.ID != fetchedAccount.ID {
		t.Errorf("Expected ID %v, got %v", account.ID, fetchedAccount.ID)
	}
}

func TestFetchAccountShouldReturn404 (t *testing.T) {

	client :=  getClient()

	// random UUID
	_, err := client.GetSingleAccount(context.TODO(), uuid.New().String())

	if err.Code != http.StatusNotFound {
		t.Errorf("Expected %v, got %v", http.StatusNotFound, err.Code)
	}
}

func TestDeleteAccountShouldReturnSuccess (t *testing.T) {

	client :=  getClient()

	account, err := createNewAccount(&client, nil)

	if err != nil {
		t.Errorf("Expected nil, got %v", err)
	}

	err = client.DeleteAccount(context.TODO(), *account)

	if err != nil {
		t.Errorf("Expected nil, got %v", err.AsMessage())
	}
}

func getClient() Client {
	return Client{
		BaseURL: "http://accountapi:8080/v1/organisation/accounts",
	}
}

func createNewAccount(c *Client, a *dto.Account) (*dto.Account, *errs.ApiError) {

	req := dto.NewAccount(uuid.New().String())

	if a == nil {
		a = &req

		attr := dto.Attributes {
			AccountClassification: "Personal",	
			AccountNumber: "1234567",
			Country: "GB",
			BaseCurrency: "GBP",
			AccountMatchingOptOut: false,
			AlternativeNames: []string{"John Doe"},
			BankID: "1234567",
			BankIDCode: "GBDSC",
			Bic: "NWBKGB22",
			Iban: "GB11NWBK40030041426819",
			JointAccount: false,
			Name: []string{"John Doe"},
		}

		a.Attributes = attr
	} 

	account, err := c.CreateAccount(context.TODO(), *a)

	return account, err
}