# Sam Odukoya's Form3 Take Home Exercise

This is a client library meant to consume the FakeAccount API defined in the [docker-compose.yml](docker-compose.yml) file. More info on the API can be found here (http://api-docs.form3.tech/api.html#organisation-accounts).


## Disclaimer
I'm new to Go. I just started working with it during a gradual decongestion of a monolith backend.


## Test guidelines
As required, the app can be run by building and running the container using this command `docker-compose up --build` and the test results for the library will be the last set of output in the logs, or in detached mode `docker-compose up --build -d` and output the logs using `docker-compose logs api-client`.

### Library Usage example
```go
package main

import (
  "fmt"
  "github.com/AyodejiO/fakeapi"
  "golang.org/x/net/context"
)

func main() {
  // initiliase the client
  client := fakeapi.Client{
		BaseURL: "url",
	}

  // create an account using the Account structure
  account := fakeapi.dto.Account{} 

  // Create a new account
  newAccount, err = client.CreateAccount(context.TODO(), account) // returns *dto.Account{}, errs.ApiError

  // Get all accounts
  // you can add filter to the get by taking advantage of the Filter struct. 
  params := fakeapi.dto.Filter{}

  allAccounts, err = client.GetAllAccounts(context.TODO(), params)  // returns *dto.AccountCollection{[]dto.AccountRequest}, errs.ApiError

  // Fetch individual account
  singleAccount, err = client.GetSingleAccount(context.TODO(), "valid-account-uuid") // returns *dto.Account{}, errs.ApiError

  // Delete account
  err = client.DeleteAccount(context.TODO(), account) // returns *errs.ApiError
  err = client.DeleteAccount(context.TODO(), account) // returns *errs.ApiError
}
```

Error messages from the API or this client is formatted using the `errs.ApiError` struct. For example 👇

```go

 _, err =  client.CreateAccount(context.TODO(), account)
 log.Printf("Error Code %d ", err.Code) // returns the HTTP response code
 log.Printf("Error Code %s ", err.AsMessage()) // returns the HTTP error response

```

You can also pass your context to control the request lifecycle. For example 👇
```go

 ctx, cancel := context.WithCancel(context.Background())
 defer cancel()
 client.GetAllAccounts(ctx, params)
```

## How to submit your exercise

- Include your name in the README. If you are new to Go, please also mention this in the README so that we can consider this when reviewing your exercise
- Create a private [GitHub](https://help.github.com/en/articles/create-a-repo) repository, by copying all files you deem necessary for your submission
- [Invite](https://help.github.com/en/articles/inviting-collaborators-to-a-personal-repository) @form3tech-interviewer-1 to your private repo
- Let us know you've completed the exercise using the link provided at the bottom of the email from our recruitment team

## License

Copyright 2019-2022 Form3 Financial Cloud

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
