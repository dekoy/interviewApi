package dto

import (
	"net/url"
	"strconv"
)

type FilterAttributes struct {
	BankIdCode string `json:"bank_id_code"`
	BankId string `json:"bank_id"`
	AccountNumber string   `json:"account_number"`
	Iban string `json:"iban"`
	CustomerId string `json:"customer_id"`
	Country string `json:"country"`
}

type Filter struct {
	PageNumber int
	PageSize int
	FilterAttributes FilterAttributes
}

func (filter Filter) ToEncodedString() string {
	encodedString := "?"

	if filter.PageSize == 0 {
		filter.PageSize = 10
	}

	params := url.Values{}

	params.Add("page[number]", strconv.Itoa(filter.PageNumber))
	params.Add("page[size]", strconv.Itoa(filter.PageSize))

	if filter.FilterAttributes.BankIdCode != "" {
		params.Add("filter[bank_id_code]", filter.FilterAttributes.BankIdCode)
	}

	if filter.FilterAttributes.BankId != "" {
		params.Add("filter[bank_id]", filter.FilterAttributes.BankId)
	}

	if filter.FilterAttributes.AccountNumber != "" {
		params.Add("filter[account_number]", filter.FilterAttributes.AccountNumber)
	}

	if filter.FilterAttributes.Iban != "" {
		params.Add("filter[iban]", filter.FilterAttributes.Iban)
	}

	if filter.FilterAttributes.CustomerId != "" {
		params.Add("filter[customer_id]", filter.FilterAttributes.CustomerId)
	}

	if filter.FilterAttributes.Country != "" {
		params.Add("filter[country]", filter.FilterAttributes.Country)
	}

	encodedString += params.Encode()
	return encodedString
}