package dto

import (
	"net/url"
	"testing"
)

func TestFilterShouldReturnEncodedStringParams (t *testing.T) {
	filter := Filter{
		PageNumber: 1,
		PageSize: 10,
		FilterAttributes: FilterAttributes{
			BankIdCode: "BNK",
			BankId: "123",
			AccountNumber: "123456",
			Iban: "BNK123456",
			CustomerId: "999",
			Country: "GB",
		},
	}

	encodedString, _ := url.QueryUnescape(filter.ToEncodedString())
	
	expected := "?filter[account_number]=123456&filter[bank_id]=123&filter[bank_id_code]=BNK&filter[country]=GB&filter[customer_id]=999&filter[iban]=BNK123456&page[number]=1&page[size]=10"
	
	if encodedString != expected {
		t.Errorf("Expected %s, got %s", expected, encodedString)
	}
}