package dto

import (
	"encoding/json"

	"github.com/google/uuid"
)

type Attributes struct {
	AccountClassification   string  `json:"account_classification,omitempty"`
	AccountMatchingOptOut   bool    `json:"account_matching_opt_out,omitempty"`
	AccountNumber           string   `json:"account_number,omitempty"`
	AlternativeNames        []string `json:"alternative_names,omitempty"`
	BankID                  string   `json:"bank_id,omitempty"`
	BankIDCode              string   `json:"bank_id_code,omitempty"`
	BaseCurrency            string   `json:"base_currency,omitempty"`
	Bic                     string   `json:"bic,omitempty"`
	Country                 string  `json:"country,omitempty"`
	Iban                    string   `json:"iban,omitempty"`
	JointAccount            bool    `json:"joint_account,omitempty"`
	Name                    []string `json:"name,omitempty"`
	SecondaryIdentification string   `json:"secondary_identification,omitempty"`
	Status                  *string  `json:"status,omitempty"`
	Switched                *bool    `json:"switched,omitempty"`
	UserDefinedData []struct {
		Key   string `json:"key"`
		Value string `json:"value"`
	} `json:"user_defined_data"`
	ValidationType      string `json:"validation_type"`
	ReferenceMask       string `json:"reference_mask"`
	AcceptanceQualifier string `json:"acceptance_qualifier"`
}

type DataObject struct {
	Attributes     Attributes `json:"attributes,omitempty"`
	ID             string             `json:"id,omitempty"`
	OrganisationID string             `json:"organisation_id,omitempty"`
	Type           string             `json:"type,omitempty"`
	Version        *int64             `json:"version,omitempty"`
}

type Account struct {
	Attributes     Attributes `json:"attributes,omitempty"`
	ID             string             `json:"id,omitempty"`
	OrganisationID string             `json:"organisation_id,omitempty"`
	Type           string             `json:"type,omitempty"`
	Version        *int64             `json:"version,omitempty"`
}

type AccountRequest struct {
	Data DataObject `json:"data"`
}

type Pagination struct {
	First string `json:"first"`
	Last string `json:"last"`
	Self string `json:"self"`
}

type AccountCollection struct {
	Data []AccountRequest `json:"data"`
	Links Pagination `json:"links"`
}

func (a Account) ToRequestObj() AccountRequest {
	return AccountRequest{
		Data: DataObject(a),
	}
}

func (a AccountRequest) ToAccountObj() Account {
	return Account(a.Data)
}

func (a AccountRequest) ToJson() ([]byte, error ) {
	body, err := json.Marshal(a)
	
	if err != nil {
		return nil, err
	}

	return body, nil
}

func NewAccount(orgID string) Account {
	return Account{
			ID: uuid.New().String(),
			Type: "accounts",
			OrganisationID: orgID,
	}
}

