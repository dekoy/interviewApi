package errs

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Error struct {
	Message string `json:"error_message"`
}

type ApiError struct {
	Code int
	Error *Error
}

func (e ApiError) AsMessage() string {
	return e.Error.Message
}

func NewApiClientError(code int, message string) *ApiError {
	return &ApiError{
		Code: code, 
		Error: NewError(message),
	}
}

func NewUnexpectedError(message string) *ApiError {
	return &ApiError{
		Code: http.StatusInternalServerError, 
		Error: &Error{
			Message: fmt.Sprintf("Unexpected error: %v", message),
		},
	}
}

func NewError(message string) *Error {
	var err Error

	json.Unmarshal([]byte(message), &err)

	return &err
}
