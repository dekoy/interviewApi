package errs

import (
	"net/http"
	"testing"
)

func TestErrorShouldReturnNewApiErrorInstance (t *testing.T) {
	msg := `{"error_message":"Bad Request"}`
	err := NewApiClientError(http.StatusBadRequest, msg)

	if err.AsMessage() != "Bad Request" {
		t.Errorf("Expected %s, got %s", "Bad Request", err.AsMessage())
	}

	if err.Code != http.StatusBadRequest {
		t.Errorf("Expected %v, got %v", http.StatusBadRequest, err.Code)
	}
}

func TestErrorShouldReturnNewUnexpectedErrorInstance (t *testing.T) {
	msg := "App Panic"
	err := NewUnexpectedError(msg)

	if err.AsMessage() != "Unexpected error: App Panic" {
		t.Errorf("Expected %s, got %s", "Unexpected error: App Panic", err.AsMessage())
	}

	if err.Code != http.StatusInternalServerError {
		t.Errorf("Expected %v, got %v", http.StatusInternalServerError, err.Code)
	}
}